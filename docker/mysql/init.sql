DROP PROCEDURE IF EXISTS create_and_init_database_if_not_exists;

DELIMITER //
CREATE PROCEDURE create_and_init_database_if_not_exists()
begin
  IF (SELECT count(*)
      from (SELECT *
            FROM INFORMATION_SCHEMA.TABLES
            WHERE table_name LIKE 'users'
              AND table_schema = 'spring_authentication') as MyResult) = 0 THEN
    create table users
    (
      username varchar(50)  not null primary key,
      password varchar(500) not null,
      enabled  boolean      not null
    );

    create table authorities
    (
      username  varchar(50) not null,
      authority varchar(50) not null
    );

    ALTER TABLE authorities
      ADD CONSTRAINT fk_authorities_users FOREIGN KEY (`username`)
        REFERENCES users (username);

    create unique index ix_auth_username on authorities (username, authority);

    create table user_login_history
    (
      username   varchar(50) not null,
      last_login timestamp,
      success    boolean     not null
    );

    ALTER TABLE authorities
      ADD CONSTRAINT fk_user_login_history_users FOREIGN KEY (`username`)
        REFERENCES users (username);

    create unique index ix_user_login_history on user_login_history (username, last_login);

    INSERT INTO spring_authentication.users (username, password, enabled)
    VALUES ('User 1', '$2a$10$JlhKV3EonrRdtMP5RHRk..hjQxHgk3n8qEncMC.iLAciA6ymbMFv6', 1);
    INSERT INTO spring_authentication.users (username, password, enabled)
    VALUES ('User 2', '$2a$10$1sa7Oz.IPDktcDMwWQuG4.tHDNFS1L8b3W.9YWTS4u3UBkbZJoouu', 1);
    INSERT INTO spring_authentication.users (username, password, enabled)
    VALUES ('User 3', '$2a$10$O8q0BehRe8zEAX7sk/ERiuvRVTqCoXMgPdY0cS9jng.XTE5jD8S1u', 1);
    INSERT INTO spring_authentication.users (username, password, enabled)
    VALUES ('Admin', '$2a$10$pEmleoZTigTBRcxVUS7hIe5KGofr91Qe5WnxdVsSXZMD38wcdLcv6', 1);

    INSERT INTO spring_authentication.authorities (username, authority) VALUES ('User 1', 'ROLE_CONTENT_MANAGER');
    INSERT INTO spring_authentication.authorities (username, authority) VALUES ('User 2', 'ROLE_CONTENT_MANAGER');
    INSERT INTO spring_authentication.authorities (username, authority) VALUES ('User 2', 'ROLE_USER');
    INSERT INTO spring_authentication.authorities (username, authority) VALUES ('User 3', 'ROLE_USER');
    INSERT INTO spring_authentication.authorities (username, authority) VALUES ('Admin', 'ROLE_ADMIN');

  END IF;
END //

call create_and_init_database_if_not_exists();