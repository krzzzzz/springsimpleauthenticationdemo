## Usage

* start mysql docker
```
docker run -p 3306:3306 -e MYSQL_ROOT_PASSWORD=Je77%%g21 -e MYSQL_DATABASE=spring_authentication mysql:latest
```
* create the starter schema: pls run the docker/mysql/init.sql content in any mysql client
* add kry-spring.local to the 127.0.0.1 (/etc/hosts or  windows windows/System32/drivers/et/hosts)
* start the spring application with maven (or import the maven project into any IDE)
```
mvn spring-boot:run
```
* open the url in browser
```
http://kry-spring.local:8080/
```
* users:

| username | pwd | role |
|---------|-----|------|
| Admin  | adminPass | ADMIN |
| User 1 | user1Pass | CONTENT_MANAGER |
| User 2 | user2Pass | CONTENT_MANAGER, USER |
| User 3 | user3Pass | USER |

## tested on:
- java 8, 12
- starter url: http://kry-spring.local:8080/
- linux, windows 10

password encoder: BCryptPasswordEncoder

checked by sonarqube according to this
https://dzone.com/articles/code-analysis-part-3-sonarqube-alternatives

```docker pull sonarqube``` 

```docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube```

sonarqube check by maven (in my case)

```mvn sonar:sonar -Dsonar.projectKey=fa77c7dfd9927b30bff640de609760983e9845ec -Dsonar.host.url=http://localhost:9000 -Dsonar.login=fa77c7dfd9927b30bff640de609760983e9845ec```



## Próbafeladat

### Általános leírás

A feladat célja egy egyszerű, beléptetéssel és jogosultságkezeléssel ellátott weboldal létrehozása.
**OK**

A felhasználó a neve és jelszava megadásával tud belépni az oldalra.
**OK**

A felhasználó adatait adatbázisban tároljuk. 
**OK**

Amennyiben a beléptetés során a felhasználó háromszor rossz jelszót ad meg, a negyedik alkalommal a név és jelszó mező mellett egy CAPTCHA kép feladványra is válaszolnia
kell.
**OK**


A belépett felhasználó egy központi adminisztrációs oldalra kerül, melynek menüjében a felhasználó csak azokat az aloldalakat láthatja, melyekhez jogosultsággal rendelkezik.
**OK**

A menüben a kilépés lehetőségre kattintva a felhasználó ki tud jelentkezni a rendszerből.
**OK**

## Funkcionális leírás

### Belépés oldal

A belépés oldal egy űrlapot (form) tartalmaz.
**OK**

#### A belépés űrlap elemei

```
● Felhasználónév
● Jelszó
● Belépés gomb
```
**OK**


#### A működés leírása

A felhasználó kitölti a felhasználónév és jelszó mezőt, majd a belépés gombra kattint
**OK**

A rendszer összeveti a megadott adatokat az adatbázisban tárolt felhasználói adatokkal
* Amennyiben az adatok megegyeznek, a felhasználó beléptetésre kerül, és a rendszer
továbbirányítja a központi adminisztrációs oldalra.
**OK**
* Amennyiben az adatok nem stimmelnek, a felhasználót visszairányítjuk a belépési
oldalra.
**OK**

▪ Amennyiben a felhasználó 3 alkalommal helytelen jelszót ad meg, a soron következő
alkalmakkal egy CAPTCHA képre is választ kell adnia
**OK**

#### Szerepkörök

Minden felhasználó rendelkezik egy vagy több szerepkörrel. 
**OK**

Amennyiben egy felhasználó több szerepkörrel is rendelkezik, a jogosultságok unióját kell képezni, azaz a felhasználó megkapja
valamennyi hozzárendelt szerepkör jogosultságát.
**OK**

A rendszer három szerepkört definiál:
* Adminisztrátor Látja az adminisztrátorok aloldalát, és az összes többi aloldalt is.
**OK**

* Tartalomszerkesztő Látja a tartalomszerkesztők aloldalát.
**OK**

* Bejelentkezett felhasználó Látja a bejelentkezett felhasználók aloldalát
**OK**

#### Követelmények:

* Minden felhasználói adat, beleértve a session-t is, adatbázisban kerüljön tárolásra.
**OK**

* A CAPTCHA kép generáláshoz felhasználható harmadik fél által írt script, vagy webes szolgáltatás is
**OK**


### Központi adminisztrációs oldal

Az oldal elemei:
* Menü
Megjeleníti azoknak az aloldalaknak a linkjeit, amelyek megtekintéséhez a felhasználónak
joga van. 
**OK**

Megjeleníti a kilépés gombot.
**OK**

* Belépési adatok:
Megjeleníti a felhasználónevet, a felhasználó csoportjait és az utolsó belépés időpontját.
**OK**

### Tartalomszerkesztők aloldala

Ezt az aloldalt csak a „tartalomszerkesztők” szerepkörrel rendelkező felhasználók láthatják
**OK**
### Bejelentkezett felhasználók aloldala

Ezt az aloldalt csak a „bejelentkezett felhasználó” szerepkörrel rendelkező felhasználók láthatják
**OK**

## Felhasználók

A rendszerben az alábbi felhasználókat kell létrehozni:
Felhasználónév Szerepkörök
Admin Adminisztrátor
* User 1 Tartalomszerkesztő + Bejelentkezett felhasználó
**OK**
* User 2 Tartalomszerkesztő
**OK**
* User 3 Bejelentkezett felhasználó
**OK**
