INSERT INTO users (username, password, enabled)
VALUES ('Admin', '$2a$10$pEmleoZTigTBRcxVUS7hIe5KGofr91Qe5WnxdVsSXZMD38wcdLcv6', 1);
INSERT INTO users (username, password, enabled)
VALUES ('User 1', '$2a$10$JlhKV3EonrRdtMP5RHRk..hjQxHgk3n8qEncMC.iLAciA6ymbMFv6', 1);
INSERT INTO users (username, password, enabled)
VALUES ('User 2', '$2a$10$1sa7Oz.IPDktcDMwWQuG4.tHDNFS1L8b3W.9YWTS4u3UBkbZJoouu', 1);
INSERT INTO users (username, password, enabled)
VALUES ('User 3', '$2a$10$O8q0BehRe8zEAX7sk/ERiuvRVTqCoXMgPdY0cS9jng.XTE5jD8S1u', 1);

INSERT INTO authorities (username, authority)
VALUES ('Admin', 'ROLE_ADMIN');
INSERT INTO authorities (username, authority)
VALUES ('User 1', 'ROLE_CONTENT_MANAGER');
INSERT INTO authorities (username, authority)
VALUES ('User 2', 'ROLE_CONTENT_MANAGER');
INSERT INTO authorities (username, authority)
VALUES ('User 2', 'ROLE_USER');
INSERT INTO authorities (username, authority)
VALUES ('User 3', 'ROLE_USER');