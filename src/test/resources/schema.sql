create table users
(
  username varchar(50)  not null primary key,
  password varchar(500) not null,
  enabled  boolean      not null
);

create table authorities
(
  username  varchar(50) not null,
  authority varchar(50) not null
);

ALTER TABLE authorities
  ADD CONSTRAINT fk_authorities_users FOREIGN KEY (`username`)
    REFERENCES users (username);

create unique index ix_auth_username on authorities (username, authority);

create table user_login_history
(
  username   varchar(50) not null,
  last_login timestamp,
  success    boolean     not null
);

ALTER TABLE authorities
  ADD CONSTRAINT fk_user_login_history_users FOREIGN KEY (`username`)
    REFERENCES users (username);

create unique index ix_user_login_history on user_login_history (username, last_login);