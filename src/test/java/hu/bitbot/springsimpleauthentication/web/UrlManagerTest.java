package hu.bitbot.springsimpleauthentication.web;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import static hu.bitbot.springsimpleauthentication.web.UrlManager.ADMIN_MAIN_PAGE;
import static hu.bitbot.springsimpleauthentication.web.UrlManager.CONTENT_MANAGER_MAIN_PAGE;
import static hu.bitbot.springsimpleauthentication.web.UrlManager.USER_MAIN_PAGE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;

class UrlManagerTest {

	@Test
	void whenGetCurrentUrlListCalledWithNull_returnEmpty() {
		assertThat(new UrlManager(null).getCurrentUrlList(), empty());
	}

	@Test
	void whenGetCurrentUrlListCalledForAdmin_returnAdminUrlList() {
		String[] roles = { "ROLE_ADMIN" };
		assertThat(new UrlManager(Arrays.asList(roles)).getCurrentUrlList(),
				contains(ADMIN_MAIN_PAGE, CONTENT_MANAGER_MAIN_PAGE, USER_MAIN_PAGE));
	}

	@Test
	void whenGetCurrentUrlListCalledForContentManagerOnly_returnContentManagerUrlOnly() {
		String[] roles = { "ROLE_CONTENT_MANAGER" };
		assertThat(new UrlManager(Arrays.asList(roles)).getCurrentUrlList(),
				contains(CONTENT_MANAGER_MAIN_PAGE));
	}

	@Test
	void whenGetCurrentUrlListCalledForContentManagerAndUser_returnBothUrls() {
		String[] roles = { "ROLE_CONTENT_MANAGER", "ROLE_USER" };
		assertThat(new UrlManager(Arrays.asList(roles)).getCurrentUrlList(),
				contains(CONTENT_MANAGER_MAIN_PAGE, USER_MAIN_PAGE));
	}
}