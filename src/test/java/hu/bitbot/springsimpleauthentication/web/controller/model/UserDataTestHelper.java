package hu.bitbot.springsimpleauthentication.web.controller.model;

public class UserDataTestHelper {

	public static void setAuthenticationFailureCount2Limit(UserData userData) {
		for (int i = 0; i < UserData.MAX_FAILURE_COUNT; i++) {
			userData.authenticationFailure();
		}
	}
}
