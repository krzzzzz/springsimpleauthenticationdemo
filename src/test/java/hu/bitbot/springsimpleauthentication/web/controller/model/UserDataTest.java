package hu.bitbot.springsimpleauthentication.web.controller.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class UserDataTest {

	UserData userData;

	@BeforeEach
	public void setup() {
		userData = new UserData();
	}

	@Test
	void whenAuthenticationFailure_thenIncreaseTheCounter() {
		userData.authenticationFailure();
		assertThat(userData.getAuthenticationFailureCount(), is(1));
		assertThat(userData.isRecaptchaRequired(), is(false));
	}

	@Test
	void whenAuthenticationFailureCalledMaxFailureCountTimes_thenIsRecaptchaRequiredIsTrue() {
		UserDataTestHelper.setAuthenticationFailureCount2Limit(userData);

		assertThat(userData.getAuthenticationFailureCount(), is(UserData.MAX_FAILURE_COUNT));
		assertThat(userData.isRecaptchaRequired(), is(true));
	}


	@Test
	void wneResetAuthenticationFailure_thenCounterZero() {
		userData.authenticationFailure();
		userData.authenticationFailure();

		userData.resetAuthenticationFailure();
		assertThat(userData.getAuthenticationFailureCount(), is(0));
	}

}