package hu.bitbot.springsimpleauthentication.captcha;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import hu.bitbot.springsimpleauthentication.CaptchaSettings;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CaptchaServiceTest {

	@InjectMocks
	CaptchaService captchaService;

	@Mock
	private CaptchaSettings captchaSettings;

	@Mock
	private RestTemplate restTemplate;

	@Mock
	private HttpServletRequest request;

	@Test
	void whenProcessResponseCalledNull_thenThrowReCaptchaInvalidException() {
		Assertions.assertThrows(ReCaptchaInvalidException.class, () -> captchaService.processResponse(null));
	}

	@Test
	void whenProcessResponseCapthcaServerIsUnavailable_thenThrowReCaptchaUnavailableException() {
		Assertions.assertThrows(ReCaptchaUnavailableException.class, () -> {
					when(restTemplate.getForObject(any(), any())).thenThrow(RestClientException.class);
					captchaService.processResponse("demo");
				}
		);
	}

	@Test
	void whenProcessResponseInvalidResponse_thenThrowReCaptchaInvalidException() {
		Assertions.assertThrows(ReCaptchaInvalidException.class, () -> {
					when(restTemplate.getForObject(any(), any())).thenReturn(GoogleResponse.builder().success(false).build());
					captchaService.processResponse("demo");
				}
		);
	}

	@Test
	void whenProcessResponseValidResponse_thenNoException()
			throws ReCaptchaUnavailableException, ReCaptchaInvalidException {
		when(restTemplate.getForObject(any(), any())).thenReturn(GoogleResponse.builder().success(true).build());

		captchaService.processResponse("demo");
	}
}