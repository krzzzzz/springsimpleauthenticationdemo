package hu.bitbot.springsimpleauthentication;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import hu.bitbot.springsimpleauthentication.captcha.GoogleResponse;
import hu.bitbot.springsimpleauthentication.captcha.ReCaptchaInvalidException;
import hu.bitbot.springsimpleauthentication.security.CustomAuthenticationFailureHandler;
import hu.bitbot.springsimpleauthentication.web.UrlManager;
import hu.bitbot.springsimpleauthentication.web.controller.model.UserData;
import hu.bitbot.springsimpleauthentication.web.controller.model.UserDataTestHelper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.ResultMatcher.matchAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class MainControllerTest {
	@MockBean
	RestTemplate restTemplate;
	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;

	@BeforeEach
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	@Test
	void whenGetRoot_thenRedirectToLogin() throws Exception {
		mvc.perform(get("/"))
				.andExpect(matchAll(
						redirectedUrl("http://localhost/login.html"),
						status().is3xxRedirection()

				));
	}

	@Test
	void whenGetHomepage_thenRedirectToLogin() throws Exception {
		mvc.perform(get("/homepage.html"))
				.andExpect(matchAll(
						redirectedUrl("http://localhost/login.html"),
						status().is3xxRedirection()

				));
	}

	@Test
	void whenGetLogin_then200Ok() throws Exception {
		mvc.perform(get("/login.html"))
				.andExpect(status().isOk())
				.andExpect(content().contentType("text/html;charset=UTF-8"));
	}

	@Test
	void whenPostPerformLoginWithWrongData_thenRedirectToLogin() throws Exception {
		MockHttpSession mockHttpSession = new MockHttpSession();
		mockHttpSession.setAttribute(UserData.SESSION_NAME, new UserData());
		mvc.perform(post(UrlManager.PERFORM_LOGIN).session(mockHttpSession)
				.with(user("admin").password("pass").roles("USER", "ADMIN"))
				.param("username", "John Doe")
				.param("password", "pwd"))
				.andDo(print())
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("login.html"))
				.andExpect(unauthenticated());

		assertThat(mockHttpSession.getAttribute("error"), is(CustomAuthenticationFailureHandler.ERROR_MESSAGE));
		assertThat(((UserData) mockHttpSession.getAttribute(UserData.SESSION_NAME)).getAuthenticationFailureCount(),
				is(1));
	}

	@Test
	void whenPostPerformLoginWithProperDataLimitTimesAndNoCaptcha_thenRedirectToLoginAndEmptyCaptchaErrorInSession()
			throws Exception {
		final String user = "Admin";
		final String pwd = "adminPass";
		MockHttpSession mockHttpSession = new MockHttpSession();
		UserData userData = new UserData();
		UserDataTestHelper.setAuthenticationFailureCount2Limit(userData);
		mockHttpSession.setAttribute(UserData.SESSION_NAME, userData);
		mvc.perform(post(UrlManager.PERFORM_LOGIN).session(mockHttpSession)
				.param("username", user)
				.param("password", pwd))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(unauthenticated())
				.andExpect(forwardedUrl("login.html"));

		assertThat(mockHttpSession.getAttribute("error"), is(ReCaptchaInvalidException.EMPTY_CAPTCHA));
	}

	@Test
	void whenPostPerformLoginWithProperDataLimitTimesAndCaptchaPresents_thenRedirectToLoginAndInvalidCaptchaErrorInSession()
			throws Exception {
		final String user = "Admin";
		final String pwd = "adminPass";
		MockHttpSession mockHttpSession = new MockHttpSession();
		UserData userData = new UserData();
		UserDataTestHelper.setAuthenticationFailureCount2Limit(userData);
		mockHttpSession.setAttribute(UserData.SESSION_NAME, userData);
		GoogleResponse googleResponse = GoogleResponse.builder().success(false).build();
		when(restTemplate.getForObject(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(googleResponse);

		mvc.perform(post(UrlManager.PERFORM_LOGIN).session(mockHttpSession)
				.param("username", user)
				.param("password", pwd)
				.param("g-recaptcha-response", "BlahBlah_INVALIDCAPTCHA"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(unauthenticated())
				.andExpect(forwardedUrl("login.html"));

		assertThat(mockHttpSession.getAttribute("error"), is(ReCaptchaInvalidException.INVALID_CAPTCHA));
		verify(restTemplate, times(1)).getForObject(ArgumentMatchers.any(), ArgumentMatchers.any());
	}

	@Test
	void whenPostPerformLoginWithProperDataLimitTimesAndCaptchaPresentsOfflineServer_thenAuthenticatingAndNoErrorInSession()
			throws Exception {
		final String user = "Admin";
		final String pwd = "adminPass";
		MockHttpSession mockHttpSession = new MockHttpSession();
		UserData userData = new UserData();
		UserDataTestHelper.setAuthenticationFailureCount2Limit(userData);
		mockHttpSession.setAttribute(UserData.SESSION_NAME, userData);
		when(restTemplate.getForObject(ArgumentMatchers.any(), ArgumentMatchers.any()))
				.thenThrow(new RestClientException("exception"));

		mvc.perform(post(UrlManager.PERFORM_LOGIN).session(mockHttpSession)
				.param("username", user)
				.param("password", pwd)
				.param("g-recaptcha-response", "BlahBlah_INVALIDCAPTCHA"))
				.andDo(print())
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("homepage.html"))
				.andExpect(authenticated());

		assertThat(mockHttpSession.getAttribute("error"), is(nullValue()));
		assertThat(userData.getRoleNameList(), contains("ROLE_ADMIN"));
		assertThat(userData.getUsername(), is(user));
		assertThat(userData.isRecaptchaRequired(), is(false));
		assertThat(userData.getAuthenticationFailureCount(), is(0));
		verify(restTemplate, times(1)).getForObject(ArgumentMatchers.any(), ArgumentMatchers.any());
	}

	@Test
	void whenPostPerformLoginWithProperData_thenRedirectToHomePage() throws Exception {
		final String user = "Admin";
		final String pwd = "adminPass";
		MockHttpSession mockHttpSession = new MockHttpSession();
		mockHttpSession.setAttribute(UserData.SESSION_NAME, new UserData());

		mvc.perform(post(UrlManager.PERFORM_LOGIN).session(mockHttpSession)
				.param("username", user)
				.param("password", pwd))
				.andDo(print())
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("homepage.html"))
				.andExpect(authenticated());

		assertThat(mockHttpSession.getAttribute("error"), is(nullValue()));
		UserData userData = (UserData) mockHttpSession.getAttribute(UserData.SESSION_NAME);
		assertThat(userData.getRoleNameList(), contains("ROLE_ADMIN"));
		assertThat(userData.getUsername(), is(user));
		assertThat(userData.isRecaptchaRequired(), is(false));
		assertThat(userData.getAuthenticationFailureCount(), is(0));
		verify(restTemplate, times(0)).getForObject(ArgumentMatchers.any(), ArgumentMatchers.any());
	}


}