package hu.bitbot.springsimpleauthentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSimpleAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSimpleAuthenticationApplication.class, args);
	}

}
