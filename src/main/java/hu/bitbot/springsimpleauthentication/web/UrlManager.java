package hu.bitbot.springsimpleauthentication.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UrlManager {
	public static final String ADMIN_MAIN_PAGE = "admin/index.html";
	public static final String CONTENT_MANAGER_MAIN_PAGE = "content-manager/index.html";
	public static final String USER_MAIN_PAGE = "user/index.html";
	public static final String HOME_PAGE = "homepage.html";
	public static final String LOGIN_PAGE = "login.html";
	public static final String PERFORM_LOGIN = "/perform_login";


	private final Map<String, String> menuUrlMap = new HashMap<>();
	private List<String> currentUrlList = new ArrayList<>();

	public UrlManager(List<String> roles) {
		menuUrlMap.put("ROLE_ADMIN", ADMIN_MAIN_PAGE);
		menuUrlMap.put("ROLE_CONTENT_MANAGER", CONTENT_MANAGER_MAIN_PAGE);
		menuUrlMap.put("ROLE_USER", USER_MAIN_PAGE);

		fillCurrentUrlList(roles);
	}

	private void fillCurrentUrlList(List<String> roles) {
		if (roles == null || roles.isEmpty()) {
			return;
		}

		if (roles.contains("ROLE_ADMIN")) {
			currentUrlList.add(ADMIN_MAIN_PAGE);
			currentUrlList.add(CONTENT_MANAGER_MAIN_PAGE);
			currentUrlList.add(USER_MAIN_PAGE);
		} else {
			currentUrlList = roles.stream().map((String role) -> menuUrlMap.get(role)).collect(Collectors.toList());
		}
	}

	public List<String> getCurrentUrlList() {
		return currentUrlList;
	}
}
