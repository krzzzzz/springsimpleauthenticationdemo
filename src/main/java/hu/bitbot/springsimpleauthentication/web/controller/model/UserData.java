package hu.bitbot.springsimpleauthentication.web.controller.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
@Scope(
		value = WebApplicationContext.SCOPE_SESSION,
		proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserData implements Serializable {
	public static final String SESSION_NAME = "scopedTarget.userData";
	public static final int MAX_FAILURE_COUNT = 3;
	private String username;
	private List<String> roleNameList = new ArrayList<>();
	private LocalDateTime lastLogin;
	@Setter(AccessLevel.NONE)
	private int authenticationFailureCount = 0;

	public Boolean isValid() {
		return !StringUtils.isEmpty(username) && roleNameList.isEmpty();
	}

	public void authenticationFailure() {
		authenticationFailureCount++;
	}

	public void resetAuthenticationFailure() {
		authenticationFailureCount = 0;
	}

	public Boolean isRecaptchaRequired() {
		return authenticationFailureCount >= MAX_FAILURE_COUNT;
	}

}
