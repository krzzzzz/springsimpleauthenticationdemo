package hu.bitbot.springsimpleauthentication.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import hu.bitbot.springsimpleauthentication.web.UrlManager;
import hu.bitbot.springsimpleauthentication.web.controller.model.UserData;

@Controller
public class MainController {

	private UserData userData;

	@GetMapping(path = UrlManager.LOGIN_PAGE)
	public ModelAndView login(HttpSession session) {
		ModelAndView mav = new ModelAndView();
		final String errorSessionName = "error";

		String error = (String) session.getAttribute(errorSessionName);
		if (error != null) {
			mav.addObject(errorSessionName, error);
			session.removeAttribute(errorSessionName);
		}
		mav.addObject("userData", userData);
		mav.setViewName("login");
		return mav;
	}

	@GetMapping(path = UrlManager.HOME_PAGE)
	public ModelAndView homepage() {
		ModelAndView mav = new ModelAndView();

		mav.addObject("userData", userData);
		mav.addObject("urlManager", new UrlManager(userData.getRoleNameList()));
		mav.setViewName("homepage");
		return mav;
	}

	@Autowired
	public void setUserData(UserData userData) {
		this.userData = userData;
	}
}
