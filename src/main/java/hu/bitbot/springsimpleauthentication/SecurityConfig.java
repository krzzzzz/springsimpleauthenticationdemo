package hu.bitbot.springsimpleauthentication;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import hu.bitbot.springsimpleauthentication.captcha.CaptchaFilterService;
import hu.bitbot.springsimpleauthentication.security.CustomLogoutSuccessHandler;

import static hu.bitbot.springsimpleauthentication.web.UrlManager.PERFORM_LOGIN;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private DataSource dataSource;
	private AuthenticationFailureHandler authenticationFailureHandler;
	private AuthenticationSuccessHandler authenticationSuccessHandler;
	private CaptchaFilterService captchaFilterService;

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication()
				.dataSource(dataSource);
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		final String adminRoleName = "ADMIN";
		http
				.csrf().disable()
				.authorizeRequests()
				.antMatchers("/admin/**").hasRole(adminRoleName)
				.antMatchers("/content-manager/**").hasAnyRole("CONTENT_MANAGER", adminRoleName)
				.antMatchers("/user/**").hasAnyRole(adminRoleName, "USER", "CONTENT_MANAGER")
				.antMatchers("/anonymous*").anonymous()
				.antMatchers("/login*").permitAll()
				.anyRequest().authenticated()
				.and()
				.formLogin()
				.loginPage("/login.html")
				.loginProcessingUrl(PERFORM_LOGIN)
				.failureUrl("/login.html?error=true")
				.failureHandler(authenticationFailureHandler)
				.successHandler(authenticationSuccessHandler)
				.and()
				.logout()
				.logoutUrl("/perform_logout")
				.deleteCookies("JSESSIONID")
				.logoutSuccessHandler(logoutSuccessHandler());

		http.addFilterBefore(captchaFilterService, UsernamePasswordAuthenticationFilter.class);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public LogoutSuccessHandler logoutSuccessHandler() {
		return new CustomLogoutSuccessHandler();
	}

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Autowired
	public void setAuthenticationSuccessHandler(AuthenticationSuccessHandler authenticationSuccessHandler) {
		this.authenticationSuccessHandler = authenticationSuccessHandler;
	}

	@Autowired
	public void setAuthenticationFailureHandler(AuthenticationFailureHandler authenticationFailureHandler) {
		this.authenticationFailureHandler = authenticationFailureHandler;
	}

	@Autowired
	public void setCaptchaFilterService(CaptchaFilterService captchaFilterService) {
		this.captchaFilterService = captchaFilterService;
	}
}
