package hu.bitbot.springsimpleauthentication;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import hu.bitbot.springsimpleauthentication.web.UrlManager;

@Configuration
public class WebConfig implements WebMvcConfigurer {
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addRedirectViewController("/", UrlManager.HOME_PAGE);
		registry.addViewController(UrlManager.ADMIN_MAIN_PAGE).setViewName("admin/index");
		registry.addViewController(UrlManager.USER_MAIN_PAGE).setViewName("user/index");
		registry.addViewController(UrlManager.CONTENT_MANAGER_MAIN_PAGE).setViewName("content-manager/index");
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
