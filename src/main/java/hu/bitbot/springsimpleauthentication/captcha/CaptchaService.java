package hu.bitbot.springsimpleauthentication.captcha;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import hu.bitbot.springsimpleauthentication.CaptchaSettings;
import lombok.extern.slf4j.Slf4j;

@Service("captchaService")
@Slf4j
public class CaptchaService implements ICaptchaService {
	private CaptchaSettings captchaSettings;

	private RestTemplate restTemplate;

	private HttpServletRequest request;

	@Override
	public void processResponse(String response) throws ReCaptchaInvalidException, ReCaptchaUnavailableException {
		if (StringUtils.isEmpty(response)) {
			throw new ReCaptchaInvalidException(ReCaptchaInvalidException.EMPTY_CAPTCHA);
		}

		final URI verifyUri = URI.create(
				String.format("https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s",
						getReCaptchaSecret(),
						response,
						getClientIP()));
		try {
			final GoogleResponse googleResponse = restTemplate.getForObject(verifyUri, GoogleResponse.class);
			log.trace("Google's response: {} ", googleResponse.toString());

			if (!googleResponse.isSuccess()) {
				throw new ReCaptchaInvalidException(ReCaptchaInvalidException.INVALID_CAPTCHA);
			}
		} catch (RestClientException rce) {
			throw new ReCaptchaUnavailableException(rce);
		}
	}

	@Override
	public String getReCaptchaSite() {
		return captchaSettings.getSite();
	}

	@Override
	public String getReCaptchaSecret() {
		return captchaSettings.getSecret();
	}

	private String getClientIP() {
		final String xfHeader = request.getHeader("X-Forwarded-For");
		if (xfHeader == null) {
			return request.getRemoteAddr();
		}
		return xfHeader.split(",")[0];
	}

	@Autowired
	public void setCaptchaSettings(CaptchaSettings captchaSettings) {
		this.captchaSettings = captchaSettings;
	}

	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Autowired
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
}
