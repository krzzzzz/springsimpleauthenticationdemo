package hu.bitbot.springsimpleauthentication.captcha;

public class ReCaptchaUnavailableException extends Exception {
	public static String SERVER_UNAVAILABLE = "Registration unavailable at this time.  Please try again later.";

	public ReCaptchaUnavailableException(Exception ex) {
		super(SERVER_UNAVAILABLE, ex);
	}
}
