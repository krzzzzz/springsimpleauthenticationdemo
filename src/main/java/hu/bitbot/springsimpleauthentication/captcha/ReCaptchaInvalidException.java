package hu.bitbot.springsimpleauthentication.captcha;

public class ReCaptchaInvalidException extends Exception {
	public static final String INVALID_CAPTCHA = "reCaptcha was not successfully validated";
	public static final String EMPTY_CAPTCHA = "empty recaptcha response";

	public ReCaptchaInvalidException(String errorMessage) {
		super(errorMessage);
	}
}
