package hu.bitbot.springsimpleauthentication.captcha;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import hu.bitbot.springsimpleauthentication.web.UrlManager;
import hu.bitbot.springsimpleauthentication.web.controller.model.UserData;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CaptchaFilterService extends GenericFilterBean {

	private CaptchaService captchaService;

	private void addErrorToSession(ServletRequest servletRequest, Throwable ex) {
		if (servletRequest instanceof HttpServletRequest) {
			((HttpServletRequest) servletRequest).getSession().setAttribute("error", ex.getMessage());
		}
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		String recapthcaString = servletRequest.getParameter("g-recaptcha-response");

		UserData userData = getUserDataFromSession(servletRequest);


		if (userData != null
				&& userData.isRecaptchaRequired()
				&& shouldFilter(servletRequest)) {
			try {
				captchaService.processResponse(recapthcaString);
				filterChain.doFilter(servletRequest, servletResponse);
			} catch (ReCaptchaInvalidException e) {
				log.debug("invalid or missing recaptcha {}", e.getMessage());
				addErrorToSession(servletRequest, e);
				servletRequest.getRequestDispatcher(UrlManager.LOGIN_PAGE).forward(servletRequest, servletResponse);
			} catch (ReCaptchaUnavailableException e) {
				log.warn("recapcha service unavailable");
				filterChain.doFilter(servletRequest, servletResponse);
			}

		} else {
			filterChain.doFilter(servletRequest, servletResponse);
		}
	}

	private UserData getUserDataFromSession(ServletRequest servletRequest) {
		UserData rv = null;
		if (servletRequest instanceof HttpServletRequest) {
			rv = (UserData) ((HttpServletRequest) servletRequest).getSession().getAttribute(UserData.SESSION_NAME);
		}

		return rv;
	}

	private boolean shouldFilter(ServletRequest request) {
		if (request instanceof HttpServletRequest) {
			String path = ((HttpServletRequest) request).getRequestURI();
			return path.startsWith(UrlManager.PERFORM_LOGIN);
		}
		return false;
	}

	@Autowired
	public void setCaptchaService(CaptchaService captchaService) {
		this.captchaService = captchaService;
	}
}
