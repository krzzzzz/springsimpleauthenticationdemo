package hu.bitbot.springsimpleauthentication.captcha;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GoogleResponse {
	@JsonProperty("success")
	private boolean success;
	@JsonProperty("challenge_ts")
	private String challengeTs;
	@JsonProperty("hostname")
	private String hostname;
	@JsonProperty("error-codes")
	private ErrorCode[] errorCodes;

	@JsonProperty("success")
	public boolean isSuccess() {
		return success;
	}

	@JsonProperty("success")
	public void setSuccess(boolean success) {
		this.success = success;
	}

	@JsonProperty("challenge_ts")
	public String getChallengeTs() {
		return challengeTs;
	}

	@JsonProperty("challenge_ts")
	public void setChallengeTs(String challengeTs) {
		this.challengeTs = challengeTs;
	}

	@JsonProperty("hostname")
	public String getHostname() {
		return hostname;
	}

	@JsonProperty("hostname")
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	@JsonProperty("error-codes")
	public ErrorCode[] getErrorCodes() {
		return errorCodes;
	}

	@JsonProperty("error-codes")
	public void setErrorCodes(ErrorCode[] errorCodes) {
		this.errorCodes = errorCodes;
	}

	@JsonIgnore
	public boolean hasClientError() {
		final ErrorCode[] errors = getErrorCodes();
		if (errors == null) {
			return false;
		}
		for (final ErrorCode error : errors) {
			switch (error) {
				case INVALID_RESPONSE:
				case MISSING_RESPONSE:
					return true;
				default:
					break;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "GoogleResponse{" + "success=" + success + ", challengeTs='" + challengeTs + '\'' + ", hostname='" +
				hostname + '\'' + ", errorCodes=" + Arrays.toString(errorCodes) + '}';
	}

	static enum ErrorCode {
		MISSING_SECRET, INVALID_SECRET, MISSING_RESPONSE, INVALID_RESPONSE;

		private static Map<String, ErrorCode> errorsMap = new HashMap<>(4);

		static {
			errorsMap.put("missing-input-secret", MISSING_SECRET);
			errorsMap.put("invalid-input-secret", INVALID_SECRET);
			errorsMap.put("missing-input-response", MISSING_RESPONSE);
			errorsMap.put("invalid-input-response", INVALID_RESPONSE);
		}

		@JsonCreator
		public static ErrorCode forValue(final String value) {
			return errorsMap.get(value.toLowerCase());
		}

	}

}
