package hu.bitbot.springsimpleauthentication.security;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import hu.bitbot.springsimpleauthentication.security.entity.UserLoginHistory;
import hu.bitbot.springsimpleauthentication.web.UrlManager;
import hu.bitbot.springsimpleauthentication.web.controller.model.UserData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private final UserLoginHistoryRepository userLoginHistoryRepository;

	private void refreshUserData(HttpServletRequest httpServletRequest, Authentication authentication) {
		String username = authentication.getName();

		UserLoginHistory latestHistoryItem =
				userLoginHistoryRepository.findTopByUsernameAndSuccessIsTrueOrderByLastLoginDesc(username);

		UserData userData = (UserData) httpServletRequest.getSession().getAttribute(UserData.SESSION_NAME);

		if (userData != null) {
			userData.resetAuthenticationFailure();
			List<String> roleNameList = authentication.getAuthorities()
					.stream()
					.map((GrantedAuthority grantedAuthority) -> grantedAuthority.getAuthority())
					.collect(Collectors.toList());
			userData.setRoleNameList(roleNameList);
			userData.setUsername(authentication.getName());

			if (latestHistoryItem != null && latestHistoryItem.getLastLogin() != null) {
				userData.setLastLogin(latestHistoryItem.getLastLogin());
			}

			httpServletRequest.getSession().setAttribute(UserData.SESSION_NAME, userData);
		}
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			Authentication authentication) throws IOException {

		refreshUserData(httpServletRequest, authentication);

		String username = authentication.getName();

		try {
			userLoginHistoryRepository.save(new UserLoginHistory(username, LocalDateTime.now(), true));
		} catch (Exception ex) {
			log.warn("error during update user login history {}", ex.getMessage());
		}
		log.debug("onAuthenticationSuccess {}", username);
		httpServletResponse.setStatus(HttpServletResponse.SC_OK);
		httpServletResponse.sendRedirect(UrlManager.HOME_PAGE);
	}
}
