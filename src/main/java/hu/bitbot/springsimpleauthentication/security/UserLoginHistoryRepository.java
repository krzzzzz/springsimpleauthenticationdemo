package hu.bitbot.springsimpleauthentication.security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hu.bitbot.springsimpleauthentication.security.entity.UserLoginHistory;

@Repository
public interface UserLoginHistoryRepository extends CrudRepository<UserLoginHistory, String> {
	UserLoginHistory findTopByUsernameAndSuccessIsTrueOrderByLastLoginDesc(String username);
}
