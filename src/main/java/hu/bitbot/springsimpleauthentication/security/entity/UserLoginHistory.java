package hu.bitbot.springsimpleauthentication.security.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@IdClass(UserLoginHistoryId.class)
public class UserLoginHistory {
	@Id
	private String username;
	@Id
	private LocalDateTime lastLogin;

	private boolean success;

}