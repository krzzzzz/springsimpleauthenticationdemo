package hu.bitbot.springsimpleauthentication.security.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginHistoryId implements Serializable {
	@Column(nullable = false, unique = true)
	private String username;

	@Column(name = "last_login")
	private LocalDateTime lastLogin;
}
