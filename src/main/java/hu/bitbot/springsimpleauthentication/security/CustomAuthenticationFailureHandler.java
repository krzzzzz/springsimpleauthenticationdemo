package hu.bitbot.springsimpleauthentication.security;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import hu.bitbot.springsimpleauthentication.security.entity.UserLoginHistory;
import hu.bitbot.springsimpleauthentication.web.UrlManager;
import hu.bitbot.springsimpleauthentication.web.controller.model.UserData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Component
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

	public static final String ERROR_MESSAGE = "Bad credentials";
	private final UserLoginHistoryRepository userLoginHistoryRepository;

	private void refreshAuthenticationFailureCounter(HttpServletRequest httpServletRequest) {
		UserData userData = (UserData) httpServletRequest.getSession().getAttribute(UserData.SESSION_NAME);

		if (userData != null) {
			userData.authenticationFailure();
			httpServletRequest.getSession().setAttribute(UserData.SESSION_NAME, userData);
		}
	}

	private void insertRecordToHistory(HttpServletRequest httpServletRequest) {
		String username = httpServletRequest.getParameter("username");
		if (!StringUtils.isEmpty(username)) {
			userLoginHistoryRepository.save(new UserLoginHistory(username, LocalDateTime.now(), false));
		}
	}

	private void insertErrorToSession(HttpServletRequest httpServletRequest) {
		httpServletRequest.getSession().setAttribute("error", ERROR_MESSAGE);
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			AuthenticationException e) throws IOException, ServletException {
		log.warn("onAuthenticationFailure {}", e.getMessage());

		refreshAuthenticationFailureCounter(httpServletRequest);
		insertErrorToSession(httpServletRequest);

		try {
			insertRecordToHistory(httpServletRequest);
		} catch (Exception ex) {
			log.warn("error during update user login history {}", ex.getMessage());
		}

		httpServletResponse.sendRedirect(UrlManager.LOGIN_PAGE);
	}
}
